<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h2>Student Information</h2>
	<c:set var="s" value="${student}" />
	<table>
		<tr>
			<th>ID</th>
			<th>NAME</th>
			<th>AGE</th>
		</tr>
		<tr>
			<th>${s.getId}</th>
			<th>${s.getName}</th>
			<th>${s.getAge}</th>
		</tr>
	</table>

	<div class="container">
		<a href="/studentMVC"> Go Back </a>
	</div>
</body>
</html>