<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<h2>Student Information</h2>
<form:form method="POST" action="/studentMVC/adding">

	<form:label path="name">Name</form:label>
	<form:input path="name" />
	<br>
	<form:label path="age">Age</form:label>
	<form:input path="age" />
	<br>
	<form:label path="id">ID</form:label>
	<form:input path="id" />
	<br>

	<input type="submit" value="Submit" />
</form:form>

<div class="container">
	<a href="index.jsp" class="list-group-item list-group-item-action">
		Go Back </a>
</div>
</body>
</html>