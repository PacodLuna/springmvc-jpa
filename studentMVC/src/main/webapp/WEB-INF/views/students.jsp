<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
    <%
        String contextPath = request.getContextPath();
        String title = "Student List";
    %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title><%=title%></title>
</head>
<body>
<h2><%=title%></h2>
<br><br>
<table style="width:90%; margin:0 auto;">
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Name</th>
      <th scope="col">Age</th>
      <th scope="col"></th>
      <th scope="col"></th>
    </tr>
    <thead>
  <c:forEach items="${students}" var="student">     
    <tr>
      <th scope="row"><c:out value="${student.getId()}"/></th>
      <td><c:out value="${student.getName()}"/></td>
      <td><c:out value="${student.getAge()}"/></td>
      <td><a href="<%=contextPath %>/delete/<c:out value="${student.getId()}"/>">Delete</a></td>
      <td><a href="<%=contextPath %>/edit/<c:out value="${student.getId()}"/>">Modify</a></td>
    </tr>
    </c:forEach>
  </tbody>
</table>
<br><br>
<a href="<%=contextPath%>">Go Back</a>
</body>
</html>