package com.softtek.academy.repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

import com.softtek.academy.model.Student;

@Repository("studentRepository")
public class StudentRepository {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public List<Student> getAll(){
		return entityManager.createQuery("SELECT s FROM Student s", Student.class).getResultList();
	}

	public void addStudent(Student student) {
		entityManager.persist(student);
	}
	
	public void deleteStudent(int id) {
		Student s = entityManager.find(Student.class, id);
		entityManager.remove(s);
	}
	
	public Student findById(int id) {
		return entityManager.find(Student.class, id);
	}
	
	public void updateStudent(Student student) {
		student = entityManager.merge(student);
	}
}
