package com.softtek.academy.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.softtek.academy.model.Student;
import com.softtek.academy.repositories.StudentRepository;

public class StudentServiceImpl implements StudentService {

	@Qualifier("studentRepository")
	@Autowired
	StudentRepository studentRepository;

	@Override
	public List<Student> getAll() {
		return studentRepository.getAll();
	}

	@Override
	public Student getById(int id) {
		return studentRepository.findById(id);
	}

	@Override
	public void deleteStudent(int id) {
		studentRepository.deleteStudent(id);
	}

	@Override
	public void addStudent(Student s) {
		studentRepository.addStudent(s);
	}

	@Override
	public void updateStudent(Student s) {
		studentRepository.updateStudent(s);		
	}
	
	
}
