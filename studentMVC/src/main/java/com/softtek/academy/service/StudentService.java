package com.softtek.academy.service;

import java.util.List;

import com.softtek.academy.model.Student;

public interface StudentService {

	List<Student> getAll();
	Student getById(int id);
	void deleteStudent(int id);
	void addStudent(Student s);
	void updateStudent(Student s);
	
}
