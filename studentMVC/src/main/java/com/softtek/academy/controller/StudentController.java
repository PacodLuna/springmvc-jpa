package com.softtek.academy.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import com.softtek.academy.model.Student;
import com.softtek.academy.service.StudentService;

@Controller
public class StudentController extends AbstractController {

	@Autowired
	StudentService studentService;

	@GetMapping("/students")
	public String students(Model model) {
		model.addAttribute("students", (List<Student>) studentService.getAll());
		return "students";
	}

	@PostMapping("/adding")
	public String adding(Model model, @RequestParam String name, @RequestParam int id, @RequestParam int edad) {
		Student s = new Student(id, edad, name);
		model.addAttribute("name", s.getName());
		model.addAttribute("age", s.getAge());
		model.addAttribute("id", s.getId());
		model.addAttribute("student", s);
		return "student";
	}
	
	@GetMapping("/delete/{id}")
	public String deleting(Model model, @PathVariable int id) {
		studentService.deleteStudent(id);
		model.addAttribute("students", studentService.getAll());
		return "students";
	}
	
	@GetMapping("/edit/{id}")
	public String editStudent(Model model, @PathVariable int id) {
		Student s = studentService.getById(id);
		model.addAttribute("name", s.getName());
		model.addAttribute("age", s.getAge());
		model.addAttribute("id", s.getId());
		model.addAttribute("student", s);
		return "editStudent";
	}
	
	@PostMapping("/updated")
	public String updated(Model model, @RequestParam int id, @RequestParam int edad, @RequestParam String name) {
		Student s = new Student(id, edad, name);
		studentService.updateStudent(s);
		model.addAttribute("name", s.getName());
		model.addAttribute("age", s.getAge());
		model.addAttribute("id", s.getId());
		model.addAttribute("student", s);
		return "student";
	}

	@GetMapping("/addStudent")
	public String add() {
		return "addStudent";
	}

	@GetMapping({ "/", "/index" })
	public String index() {
		return "index";
	}

	@Override
	protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ModelAndView model = new ModelAndView("index");
		return model;
	}
}
